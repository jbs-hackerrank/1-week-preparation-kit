﻿using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;


class Result {

    /*
     * Complete the 'truckTour' function below.
     *
     * The function is expected to return an INTEGER.
     * The function accepts 2D_INTEGER_ARRAY petrolpumps as parameter.
     */

    public static int truckTour(List<List<int>> petrolpumps) {
        var n = petrolpumps.Count;

        var diffs = petrolpumps.Select(pump => pump[0] - pump[1]).ToList();

        for (var i = 0; i < n; i++) {
            if (isSolution(diffs, i)) {
                return i;
            }
        }

        throw new Exception("Solution not found");
    }

    private static bool isSolution(List<int> diffs, int start) {
        var gas = diffs[start];

        for (var i = (start + 1) % diffs.Count; i != start; i = (i + 1) % diffs.Count) {
            if (gas < 0) {
                return false;
            }

            gas += diffs[i];
        }

        return true;
    }

}

class Solution {

    public static void Main(string[] args) {
        TextWriter textWriter = new StreamWriter(Console.OpenStandardOutput());

        int n = Convert.ToInt32(Console.ReadLine().Trim());

        List<List<int>> petrolpumps = new List<List<int>>();

        for (int i = 0; i < n; i++) {
            petrolpumps.Add(Console.ReadLine()
                .TrimEnd()
                .Split(' ')
                .ToList()
                .Select(petrolpumpsTemp => Convert.ToInt32(petrolpumpsTemp))
                .ToList()
            );
        }

        int result = Result.truckTour(petrolpumps);

        textWriter.WriteLine(result);

        textWriter.Flush();
        textWriter.Close();
    }

}
