﻿using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{

    /*
     * Complete the 'minimumBribes' function below.
     *
     * The function accepts INTEGER_ARRAY q as parameter.
     */

    /**
     * 1 2 3 4 5 6 7 8  // 5
     * 1 2 3 5 4 6 7 8  // 5
     * 1 2 5 3 4 6 7 8  // 6
     * 1 2 5 3 6 4 7 8  // 7
     * 1 2 5 3 6 7 4 8  // 7
     * 1 2 5 3 7 6 4 8  // 8
     * 1 2 5 3 7 6 8 4  // 8
     * 1 2 5 3 7 8 6 4
     *
     * 1 2 3 4 5 6 7 8
     * 
     * 1 2 5 3 7 8 6 4
     * 1 2 3 5 7 8 6 4  3,5
     * 1 2 3 5 6 7 8 4  6,8 6,7
     * 1 2 3 4 5 6 7 8  4,8 4,7 4,6 4,5
     *
     * 2 5 1 3 4
     * 1 2 5 3 4  1,5, 1,2
     * 1 2 3 5 4  3,5
     * 1 2 3 4 5  4,5
     */
    public static void minimumBribes(List<int> q) {
        var n = q.Count;
        var individualBribes = new int[n + 1];
        var totalBribes = 0;
        int compareTo;

        for (var a = 1; a < n; a++) {
            compareTo = q[a];
            var runner = a - 1;

            while (runner >= 0 && q[runner] >= compareTo) {
                q[runner + 1] = q[runner]; // shift larger element right
                individualBribes[q[runner]] += 1;

                if (individualBribes[q[runner]] > 2) {
                    Console.WriteLine("Too chaotic");
                    return;
                }

                totalBribes += 1;

                runner--;
            }

            q[runner + 1] = compareTo;
        }

        Console.WriteLine(totalBribes);
    }

}

class Solution
{
    public static void Main(string[] args)
    {
        int t = Convert.ToInt32(Console.ReadLine().Trim());

        for (int tItr = 0; tItr < t; tItr++)
        {
            int n = Convert.ToInt32(Console.ReadLine().Trim());

            List<int> q = Console.ReadLine().TrimEnd().Split(' ').ToList().Select(qTemp => Convert.ToInt32(qTemp)).ToList();

            Result.minimumBribes(q);
        }
    }
}
