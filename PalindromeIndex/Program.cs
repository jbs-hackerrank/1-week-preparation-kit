﻿using System;
using System.IO;

class Result {

    private const int NoSolution = -1;
    private const int IsPalindrome = -2;

    /*
     * Complete the 'palindromeIndex' function below.
     *
     * The function is expected to return an INTEGER.
     * The function accepts STRING s as parameter.
     */

    public static int palindromeIndex(string s) {
        var index = findIndexToRemove(s, 0, s.Length - 1, true, true);
        return index < 0 ? NoSolution : index;
    }

    private static int findIndexToRemove(string s, int start, int end, bool needToRemove, bool isWholeWord = false) {
        while (start < end && s[start] == s[end]) {
            start++;
            end--;
        }

        if (start >= end) {
            return IsPalindrome;
        }

        if (!needToRemove) {
            return NoSolution;
        }

        var withoutStart = findIndexToRemove(s, start + 1, end, false);

        if (withoutStart == IsPalindrome) {
            return start;
        }

        var withoutEnd = findIndexToRemove(s, start, end - 1, false);

        if (withoutEnd == IsPalindrome) {
            return end;
        }

        return NoSolution;

    }


}

class Solution {

    public static void Main(string[] args) {
        TextWriter textWriter = new StreamWriter(Console.OpenStandardOutput());

        int q = Convert.ToInt32(Console.ReadLine().Trim());

        for (int qItr = 0; qItr < q; qItr++) {
            string s = Console.ReadLine();

            int result = Result.palindromeIndex(s);

            textWriter.WriteLine(result);
        }

        textWriter.Flush();
        textWriter.Close();
    }

}
