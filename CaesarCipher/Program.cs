﻿using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result {

    /*
     * Complete the 'caesarCipher' function below.
     *
     * The function is expected to return a STRING.
     * The function accepts following parameters:
     *  1. STRING s
     *  2. INTEGER k
     */

    public static string caesarCipher(string s, int k) {
        var builder = new StringBuilder();
        k %= 26;

        foreach (var c in s) {
            switch (c) {
                case >= 'a' and <= 'z':
                    builder.Append(encode(c, k, 'z'));
                    break;

                case >= 'A' and <= 'Z':
                    builder.Append(encode(c, k, 'Z'));
                    break;

                default:
                    builder.Append(c);
                    break;
            }
        }

        return builder.ToString();
    }

    private static char encode(char c, int k, char z) {
        var asNumber = c + k;

        if (asNumber > z) {
            asNumber -= 26;
        }

        return (char)asNumber;
    }

}

class Solution {

    public static void Main(string[] args) {
        TextWriter textWriter = new StreamWriter(Console.OpenStandardOutput());

        int n = Convert.ToInt32(Console.ReadLine().Trim());

        string s = Console.ReadLine();

        int k = Convert.ToInt32(Console.ReadLine().Trim());

        string result = Result.caesarCipher(s, k);

        textWriter.WriteLine(result);

        textWriter.Flush();
        textWriter.Close();
    }

}
