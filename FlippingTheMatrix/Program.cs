﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

class Result {

    /*
     * Complete the 'flippingMatrix' function below.
     *
     * The function is expected to return an INTEGER.
     * The function accepts 2D_INTEGER_ARRAY matrix as parameter.
     */

    public static int flippingMatrix(List<List<int>> matrix) {
        var result = 0;
        var size = matrix.Count / 2;
        var fullSize = matrix.Count;

        for (var r = 0; r < size; r++) {
            for (var c = 0; c < size; c++) {
                var max = matrix[r][c];
                max = Math.Max(max, matrix[r][fullSize - c - 1]);
                max = Math.Max(max, matrix[fullSize - r - 1][c]);
                max = Math.Max(max, matrix[fullSize - r - 1][fullSize - c - 1]);
                result += max;
            }
        }

        return result;
    }

}

class Solution {

    public static void Main(string[] args) {
        TextWriter textWriter = new StreamWriter(Console.OpenStandardOutput());

        int q = Convert.ToInt32(Console.ReadLine().Trim());

        for (int qItr = 0; qItr < q; qItr++) {
            int n = Convert.ToInt32(Console.ReadLine().Trim());

            List<List<int>> matrix = new List<List<int>>();

            for (int i = 0; i < 2 * n; i++) {
                matrix.Add(Console.ReadLine()
                    .TrimEnd()
                    .Split(' ')
                    .ToList()
                    .Select(matrixTemp => Convert.ToInt32(matrixTemp))
                    .ToList()
                );
            }

            int result = Result.flippingMatrix(matrix);

            textWriter.WriteLine(result);
        }

        textWriter.Flush();
        textWriter.Close();
    }

}
