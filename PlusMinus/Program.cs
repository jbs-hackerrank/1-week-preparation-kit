﻿using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result {

    /*
     * Complete the 'plusMinus' function below.
     *
     * The function accepts INTEGER_ARRAY arr as parameter.
     */

    public static void plusMinus(List<int> arr) {
        double positive = 0;
        double negative = 0;
        double zero = 0;
        
        foreach (var i in arr) {
            switch (i) {
                case > 0:
                    positive++;
                    break;

                case < 0:
                    negative++;
                    break;

                default:
                    zero++;
                    break;
            }
        }

        Console.WriteLine($"{positive / arr.Count:0.000000}");
        Console.WriteLine($"{negative / arr.Count:0.000000}");
        Console.WriteLine($"{zero / arr.Count:0.000000}");
    }

}

class Solution {

    public static void Main(string[] args) {
        int n = Convert.ToInt32(Console.ReadLine().Trim());

        List<int> arr = Console.ReadLine()
            .TrimEnd()
            .Split(' ')
            .ToList()
            .Select(arrTemp => Convert.ToInt32(arrTemp))
            .ToList();

        Result.plusMinus(arr);
    }

}
