﻿using System;
using System.IO;
using System.Linq;

class Result {

    /*
     * Complete the 'superDigit' function below.
     *
     * The function is expected to return an INTEGER.
     * The function accepts following parameters:
     *  1. STRING n
     *  2. INTEGER k
     */

    public static int superDigit(string n, int k) {
        var sum = n.Sum(c => c - '0');

        var originalSuperDigit = reduce(sum);

        return reduce(k * originalSuperDigit);
    }

    private static int reduce(int value) {
        while (value > 9) {
            var newVal = 0;

            while (value > 0) {
                newVal += value % 10;
                value /= 10;
            }

            value = newVal;
        }

        return value;
    }

}

class Solution {

    public static void Main(string[] args) {
        TextWriter textWriter = new StreamWriter(Console.OpenStandardOutput());

        string[] firstMultipleInput = Console.ReadLine().TrimEnd().Split(' ');

        string n = firstMultipleInput[0];

        int k = Convert.ToInt32(firstMultipleInput[1]);

        int result = Result.superDigit(n, k);

        textWriter.WriteLine(result);

        textWriter.Flush();
        textWriter.Close();
    }

}
