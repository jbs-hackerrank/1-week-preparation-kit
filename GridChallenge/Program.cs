﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

class Result {

    /*
     * Complete the 'gridChallenge' function below.
     *
     * The function is expected to return a STRING.
     * The function accepts STRING_ARRAY grid as parameter.
     */

    public static string gridChallenge(List<string> grid) {
        if (grid.Count == 1) {
            return "YES";
        }
        
        var rows = grid.Select(s => s.OrderBy(c => c).ToList()).ToList();
        var numChars = rows[0].Count;

        for (var col = 0; col < numChars; col++) {
            for (var row = 1; row < rows.Count; row++) {
                if (rows[row][col] < rows[row - 1][col]) {
                    return "NO";
                }
            }
        }

        return "YES";
    }

}

class Solution {

    public static void Main(string[] args) {
        TextWriter textWriter = new StreamWriter(Console.OpenStandardOutput());

        int t = Convert.ToInt32(Console.ReadLine().Trim());

        for (int tItr = 0; tItr < t; tItr++) {
            int n = Convert.ToInt32(Console.ReadLine().Trim());

            List<string> grid = new List<string>();

            for (int i = 0; i < n; i++) {
                string gridItem = Console.ReadLine();
                grid.Add(gridItem);
            }

            string result = Result.gridChallenge(grid);

            textWriter.WriteLine(result);
        }

        textWriter.Flush();
        textWriter.Close();
    }

}
