﻿using System;
using System.Collections.Generic;
using System.IO;

class Solution {

    private readonly int numQueries;
    private readonly Stack<int> EnqueueStack = new();
    private readonly Stack<int> DequeueStack = new();
    public Solution(int numQueries) { this.numQueries = numQueries; }

    public void Solve() {
        for (var i = 0; i < numQueries; i++) {
            var line = Console.ReadLine()!;

            switch (line[0]) {
                case '1':
                    Enqueue(Convert.ToInt32(line.Split(" ")[1]));
                    break;

                case '2':
                    Dequeue();
                    break;

                case '3':
                    Print();
                    break;
            }
        }
    }

    private void Enqueue(int value) {
        if (DequeueStack.Count == 0 && EnqueueStack.Count == 0) {
            DequeueStack.Push(value);
            return;
        }

        EnqueueStack.Push(value);
    }

    private void Dequeue() {
        if (DequeueStack.Count == 0) {
            MoveStacks();
        }
        
        DequeueStack.Pop();
    }

    private void Print() {
        if (DequeueStack.Count == 0) {
            MoveStacks();
        }

        Console.WriteLine(DequeueStack.Peek());
    }

    private void MoveStacks() {
        while (EnqueueStack.Count > 0) {
            DequeueStack.Push(EnqueueStack.Pop());
        }
    }

    static void Main(String[] args) {
        var q = Convert.ToInt32(Console.ReadLine());
        var solution = new Solution(q);
        solution.Solve();
    }

}
