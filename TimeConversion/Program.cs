﻿using System;
using System.IO;
using System.Text.RegularExpressions;

class Result {

    /*
     * Complete the 'timeConversion' function below.
     *
     * The function is expected to return a STRING.
     * The function accepts STRING s as parameter.
     */

    public static string timeConversion(string s) {
        var match = Regex.Match(s, @"^(\d\d)(.+)(A|P)M$");
        var hour = int.Parse(match.Groups[1].Value);

        var isMorning = match.Groups[3].Value == "A";

        if (hour == 12) {
            if (isMorning) {
                hour = 0;
            }
        } else if (!isMorning) {
            hour += 12;
        }

        return $"{hour:00}{match.Groups[2].Value}";
    }

}

class Solution {

    public static void Main(string[] args) {
        TextWriter textWriter = new StreamWriter(Console.OpenStandardOutput());

        string s = Console.ReadLine();

        string result = Result.timeConversion(s);

        textWriter.WriteLine(result);

        textWriter.Flush();
        textWriter.Close();
    }

}
